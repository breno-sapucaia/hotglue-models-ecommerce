from datetime import date, datetime, timezone
from typing import List, Optional, Any, Dict, ClassVar
from typing_extensions import Literal
from unicodedata import category
from pydantic import BaseModel, validator


def convert_datetime_to_iso_8601(dt: datetime) -> str:
    return dt.isoformat(timespec="milliseconds")


def convert_date_str(dt: datetime.date) -> str:
    return dt.strftime("%Y-%m-%d")


class Location(BaseModel):
    schema_name: ClassVar[str] = "Locations"
    id: Optional[str]
    name: Optional[str]


class Category(BaseModel):
    schema_name: ClassVar[str] = "Categories"
    id: Optional[str]
    name: Optional[str]


class Supplier(BaseModel):
    id: Optional[str]
    name: Optional[str]


class Options(BaseModel):
    name: str
    value: str


class Variant(BaseModel):
    id: Optional[str]
    sku: Optional[str]
    price: Optional[float]
    sale_price: Optional[float]
    cost: Optional[float]
    available_quantity: Optional[int]
    options: Optional[List[Options]]
    image_urls: Optional[List[str]]
    barcode: Optional[str]
    width: Optional[str]
    length: Optional[str]
    depth: Optional[str]
    weight: Optional[str]
    mass_unit: Optional[str]
    distance_unit: Optional[str]
    description: Optional[str]


class Product(BaseModel):
    schema_name: ClassVar[str] = "Products"
    id: Optional[str]
    name: Optional[str]
    cost: Optional[float]
    short_description: Optional[str]
    sku: Optional[str]
    description: Optional[str]
    variants: Optional[List[Variant]]
    options: Optional[List[str]]
    location: Optional[Location]
    category: Optional[Category]
    categories: Optional[List[Category]]
    suppliers: Optional[List[Supplier]]
    published: Optional[bool]
    active: Optional[bool]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]
    image_urls: Optional[List[str]]
    tax_code: Optional[str]
    origin_country_code: Optional[str]

    class Stream:
        name = "Products"

    @validator("updated_at", "created_at")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class OrderLineItem(BaseModel):
    id: Optional[str]
    product_id: Optional[str]
    product_name: Optional[str]
    sku: Optional[str]
    quantity: Optional[float]
    unit_price: Optional[float]
    total_price: Optional[float]
    discount_amount: Optional[float]
    tax_amount: Optional[float]
    notes: Optional[str]

    class Stream:
        name = "OrderLineItems"


class Products(BaseModel):
    __root__: List[Product]


class OrderLineItems(BaseModel):
    __root__: List[OrderLineItem]


class Address(BaseModel):
    id: Optional[str]
    line1: Optional[str]
    line2: Optional[str]
    line3: Optional[str]
    city: Optional[str]
    state: Optional[str]
    postal_code: Optional[str]
    country: Optional[str]


class ShippingLines(BaseModel):
    id: Optional[str]
    code: Optional[str]
    source: Optional[str]
    carrier: Optional[str]
    subtotal: Optional[float]
    total_price: Optional[float]
    discount_amount: Optional[float]


class SalesOrder(BaseModel):
    schema_name: ClassVar[str] = "Sales Order"
    id: Optional[str]
    order_number: Optional[str]
    parent_order_id: Optional[str]
    customer_id: Optional[str]
    customer_name: Optional[str]
    customer_email: Optional[str]
    customer_company: Optional[str]
    phone: Optional[str]
    billing_name: Optional[str]
    billing_email: Optional[str]
    billing_company: Optional[str]
    billing_phone: Optional[str]
    line_items: Optional[List[OrderLineItem]]
    shipping_lines: Optional[List[ShippingLines]]
    billing_address: Optional[Address]
    shipping_company: Optional[str]
    shipping_phone: Optional[str]
    shipping_address: Optional[Address]
    fulfilled: Optional[bool]
    paid: Optional[bool]
    subtotal: Optional[float]
    total_price: Optional[float]
    total_discount: Optional[float]
    total_shipping: Optional[float]
    total_tax: Optional[float]
    carrier: Optional[str]
    tracking_number: Optional[str]
    tracking_url: Optional[str]
    delivery_status: Optional[str]
    status: Optional[str]
    currency: Optional[str]
    payment_method: Optional[str]
    requested_date: Optional[datetime]
    tags: Optional[List[Optional[str]]]
    transaction_date: Optional[datetime]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]
    order_notes: Optional[str]

    class Stream:
        name = "SalesOrders"


class SalesOrders(BaseModel):
    __root__: List[SalesOrder]


class Customer(BaseModel):
    schema_name: ClassVar[str] = "Customers"
    id: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]
    email: Optional[str]
    phone: Optional[str]
    company: Optional[str]
    address: Optional[Address]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]

    class Stream:
        name = "Customers"
    
class Customers(BaseModel):
    __root__: List[Customer]